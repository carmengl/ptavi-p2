#!/usr /bin/python3
# -*- coding: utf-8 -*-
# Carmen Gonzalez Lopez

import sys


class Calculadora():
    def opera(self, op1, op2):
        self.op1 = op1
        self.op2 = op2

    def plus(self):
        return self.op1 + self.op2

    def minus(self):
        return self.op1 - self.op2

if __name__ == "__main__":

    calc = Calculadora()
    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])

    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        calc.opera(operando1, operando2)
        result = calc.plus()
    elif sys.argv[2] == "resta":
        calc.opera(operando1, operando2)
        result = calc.minus()
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(result)
