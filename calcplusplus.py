#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Carmen Gonzalez Lopez

import sys
import calcoohija
import csv


if __name__ == "__main__":

    calcuhija = calcoohija.CalculadoraHija()

    with open(sys.argv[1], 'r') as csvfichero:
        reader = csv.reader(csvfichero)

        for posicion in reader:
            operacion = posicion[0]
            operando1 = posicion[1]

            if operacion == "suma":
                for operando2 in posicion[2:]:
                    calcuhija.opera(int(operando1), int(operando2))
                    operando1 = calcuhija.plus()
                print(operando1)
            elif operacion == "resta":
                for operando2 in posicion[2:]:
                    calcuhija.opera(int(operando1), int(operando2))
                    operando1 = calcuhija.minus()
                print(operando1)
            elif operacion == "divide":
                for operando2 in posicion[2:]:
                    calcuhija.opera(int(operando1), int(operando2))
                    operando1 = calcuhija.div()
                print(operando1)
            elif operacion == "multiplica":
                for operando2 in posicion[2:]:
                    calcuhija.opera(int(operando1), int(operando2))
                    operando1 = calcuhija.multi()
                print(operando1)
            else:
                print('No es posible realizar la operacion')

csvfichero.close()
