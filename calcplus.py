#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Carmen Gonzalez Lopez

import sys
import calcoohija


def calculate(operacion, posicion):
    operando1 = posicion[1]
    if operacion == 'suma':
        for operando2 in posicion[2:]:
            calchija.opera(int(operando1), int(operando2))
            operando1 = calchija.plus()
        print(operando1)
    elif operacion == 'resta':
        for operando2 in posicion[2:]:
            calchija.opera(int(operando1), int(operando2))
            operando1 = calchija.minus()
        print(operando1)
    elif operacion == 'divide':
        for operando2 in posicion[2:]:
            calchija.opera(int(operando1), int(operando2))
            operando1 = calchija.div()
        print(operando1)
    elif operacion == 'multiplica':
        for operando2 in posicion[2:]:
            calchija.opera(int(operando1), int(operando2))
            operando1 = calchija.multi()
        print(operando1)
    else:
        print('No es posible realizar la operacion')

if __name__ == "__main__":

    try:
        calchija = calcoohija.CalculadoraHija()
        file = open(sys.argv[1], "r")

    except ValueError:
        sys.exit("Error: Non numerical parameters")

for list in file.readlines():
    position = list.split(',')
    opera = position[0]
    calculate(opera, position)

file.close()
