#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Carmen Gonzalez Lopez

import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):

    def div(self):
        if self.op2 == 0:
            print("Division by zero is not allowed")
        else:
            return self.op1 / self.op2

    def multi(self):
        return self.op1 * self.op2

if __name__ == "__main__":

    calcuhija = CalculadoraHija()

    try:
        operando1 = int(sys.argv[1])
        operando2 = int(sys.argv[3])

    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        calcuhija.opera(operando1, operando2)
        result = calcuhija.plus()
    elif sys.argv[2] == "resta":
        calcuhija.opera(operando1, operando2)
        result = calcuhija.minus()
    elif sys.argv[2] == "division":
        calcuhija.opera(operando1, operando2)
        result = calcuhija.div()
    elif sys.argv[2] == "multiplicacion":
        calcuhija.opera(operando1, operando2)
        result = calcuhija.multi()
    else:
        sys.exit('No es posible realizar la operacion')

    print(result)
